import React from 'react';
import { View, StyleSheet, Button } from 'react-native';
import { SecureStore } from 'expo';

import t from 'tcomb-form-native';

const Form = t.form.Form;

const User = t.struct({
  email: t.String,
  username: t.String,
  password: t.String,
  terms: t.Boolean
});

const formStyles = {
  ...Form.stylesheet,
  formGroup: {
    normal: {
      marginBottom: 10
    },
  },
  controlLabel: {
    normal: {
      color: 'blue',
      fontSize: 18,
      marginBottom: 7,
      fontWeight: '600'
    },
    // the style applied when a validation error occours
    error: {
      color: 'red',
      fontSize: 18,
      marginBottom: 7,
      fontWeight: '600'
    }
  }
}

const options = {
  fields: {
    email: {
      error: 'Without an email address how are you going to reset your password when you forget it?'
    },
    password: {
      error: 'Choose something you use on a dozen other sites or something you won\'t remember'
    },
    terms: {
      label: 'Agree to Terms',
    },
  },
  stylesheet: formStyles,
};
export default class Profile extends React.Component {
  static navigationOptions = {
    title: 'Profile Settings',
  };
  constructor(props) {
    super(props);
    this.state = {
      options: options,
      value: null
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillMount = async () => {
    console.log('start');

    let username = await SecureStore.getItemAsync("username");
    let password = await SecureStore.getItemAsync("password");
    let email = await SecureStore.getItemAsync("email");
    this.setState ({
      value: {
        username: username,
        password: password,
        email: email
      }
    });
  };
onChange(value) {

    this.setState({options: options, value: value});
  }
  handleSubmit = () => {
    const value = this._form.getValue();
    alert(this._form.getValue().email);
    var username = this._form.getValue().username;
    var password = this._form.getValue().password;
    var email = this._form.getValue().email;
    console.log('value: ', value);
    SecureStore.setItemAsync('username', username);
    SecureStore.setItemAsync('password', password);
    SecureStore.setItemAsync('email', email);
  }

  render() {
    return (
      <View style={styles.container}>
        <Form
          ref={c => this._form = c}
          type={User}
          value={this.state.value}
          options={options}
        />
        <Button
          title="Sign Up!"
          onPress={this.handleSubmit}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
});

import React from 'react';
import Profile from '../components/settings/Profile';

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'app.json',
  };

  render() {

    return <Profile />;
  }
}
